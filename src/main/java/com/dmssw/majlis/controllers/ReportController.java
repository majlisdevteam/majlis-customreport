/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controllers;

import com.dmssw.majlis.config.AppParams;
import com.dmssw.majlis.entities.Report;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;
import com.dmssw.orm.controllers.DbCon;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Date;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import java.util.StringTokenizer;

/**
 * <p>
 * <b>Title</b> ReportController
 * </p>
 * <p>
 * <b>Description</b> Enter Purpose of this class
 * </p>
 * <p>
 * <b>Company</b> Data Management Systems (PVT) Ltd
 * </p>
 * <p>
 * <b>Copyright</b> Copyright (c) 2015
 * </p>
 *
 * @author Hashan Jayakody
 * @version 1.0
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class ReportController {

    private static Logger LOGGER = Logger.getLogger("InfoLogging");

    @Resource
    UserTransaction transaction;

    public ResponseData createReport(Report report) {

        ResponseData reponseData = new ResponseData();
        int result = -1;

        if (validateQuery(report.getQuery())) {

            DbCon dbCon = new DbCon();
            Connection connction = dbCon.getCon();
            try {

                String insertSql = "INSERT INTO majlis_custom_reports (REPORT_NAME, REPORT_QUERY,REPORT_STATUS) VALUES (?,?,?)";

                PreparedStatement ps = dbCon.prepareAutoId(connction, insertSql);

                ps.setString(1, report.getReportName());

                ps.setString(2, report.getQuery());
                ps.setInt(3, report.getStatus());

                dbCon.executePreparedStatement(connction, ps);

                ResultSet key = ps.getGeneratedKeys();

                int idKey = 0;

                while (key.next()) {
                    idKey = key.getInt(1);
                }

               
                result = 1;
                reponseData.setResponseData(idKey);

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    connction.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            result = 999;
        }

        reponseData.setResponseCode(result);
        return reponseData;

    }

    public ResponseData updateReport(Report report) {
        ResponseData reponseData = new ResponseData();
        int result = -1;

        if (validateQuery(report.getQuery())) {

            DbCon dbCon = new DbCon();
            Connection connction = dbCon.getCon();

            try {
                String updateSql = "UPDATE majlis_custom_reports SET REPORT_NAME='" + report.getReportName() + "', "
                        + "REPORT_QUERY='" + report.getQuery() + "', REPORT_STATUS=" + report.getStatus() + " WHERE "
                        + "REPORT_ID=" + report.getReportId();

                dbCon.save(connction, updateSql);
                result = 1;

                reponseData.setResponseData(report.getReportId());

            } catch (Exception ex) {
                result = 999;
                ex.printStackTrace();
            } finally {
                try {
                    connction.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            result = 999;
        }
        reponseData.setResponseCode(result);
        return reponseData;

    }

   

    private boolean validateQuery(String sql) {

        boolean isValid = false;
        int count = 0;

        StringTokenizer st = new StringTokenizer(sql);

        while (st.hasMoreTokens()) {
            String word = st.nextToken();
            if (word.equals("WHERE")) {
                count++;
            }
        }

        String arr[] = sql.split(" ");

        if (count < 2 && "SELECT".equals(arr[0])) {
            isValid = true;
        } else {
        }
        return isValid;
    }

    public ResponseData getReportList() {

        ResponseData reponseData = new ResponseData();
        int result = -1;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        List<Report> list = new ArrayList<Report>();

        try {
            String sql = "SELECT * FROM majlis_custom_reports";

            ResultSet rs = dbCon.search(con, sql);

            while (rs.next()) {
                Report report = new Report();
                report.setReportId(rs.getInt("REPORT_ID"));
                report.setReportName(rs.getString("REPORT_NAME"));
                report.setQuery(rs.getString("REPORT_QUERY"));
                report.setStatus(rs.getInt("REPORT_STATUS"));

                list.add(report);

            }

            result = 1;
            reponseData.setResponseData(list);
        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        reponseData.setResponseCode(result);
        return reponseData;

    }

    public ResponseData getData(String query) {

        ResponseData responseData = new ResponseData();
        int result = -1;

        System.out.println("aaaa");

        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        //String repPath = ReportService.excelPath();

        String filename = AppParams.IMG_PATH + "/" + AppParams.REPORT_PATH + "/report.xls";
        new File(filename).delete();
        System.out.println("file" + filename);
        HSSFWorkbook workbook = new HSSFWorkbook();

        try {

            System.out.println("Query getdata =>>>>>>>>>> " + query);

            String countquery = "SELECT COUNT(*) FROM (" + query + ") AS COUNT";
            System.out.println("Query" + countquery);

            ResultSet rsCount = dbCon.search(con, countquery);
            int resultCount = 0;

            while (rsCount.next()) {
                resultCount = rsCount.getInt(1);
            }

            int rsC = 0;
            int sheetRowRound = 0;

            System.out.println("Sheet COUNT >>> " + resultCount / 65500);

            HSSFSheet sheet;

            if (resultCount > 65500) {

                sheetRowRound = resultCount / 65500;

                int sheetCount = 1;
                for (int m = 0; m < sheetRowRound; m++) {

                    int lowLimit = m * 65500;
                    int upLimit = (m + 1) * 65500;

                    if (lowLimit != 0) {
                        lowLimit++;
                    }

//                    String newQuery = "SELECT * FROM "
//                            + "(SELECT S.*, ROWNUM as RN FROM ( "
//                            + query
//                            + " ) S "
//                            + ") M WHERE M.RN BETWEEN "+lowLimit+" AND "+upLimit;
                    String newQuery = query + " LIMIT " + lowLimit + " OFFSET " + upLimit;
                    System.out.println("Query2" + newQuery);
                    ResultSet rs = dbCon.search(con, newQuery);

                    String sheetName = "Sheet".concat("" + sheetCount);
                    sheet = workbook.createSheet(sheetName);

                    rsC = 1;
                    while (rs.next()) {

                        HSSFRow rowhead = sheet.createRow((int) 0);
                        HSSFRow row = sheet.createRow((int) rsC);
                        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                            rowhead.createCell(i).setCellValue(rs.getMetaData().getColumnName(i));
                            row.createCell(i).setCellValue(rs.getString(i));
                        }

                        rsC++;

                    }
                    sheetCount++;

                }

            } else {

                String sheetName = "Sheet1";
                sheet = workbook.createSheet(sheetName);
                ResultSet rs = dbCon.search(con, query);
                rsC = 1;

                while (rs.next()) {

                    HSSFRow rowhead = sheet.createRow((int) 0);
                    HSSFRow row = sheet.createRow((int) rsC);
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {

                        rowhead.createCell(i).setCellValue(rs.getMetaData().getColumnName(i));
                        row.createCell(i).setCellValue(rs.getString(i));
                    }

                    rsC++;

                }

            }

            FileOutputStream fileOut = new FileOutputStream(filename);

            workbook.write(fileOut);
            fileOut.close();
            result = 1;
            responseData.setResponseData("Success");
            dbCon.ConectionClose(con);
        } catch (Exception er) {
            result = 999;
            er.printStackTrace();

        }
        responseData.setResponseCode(result);
        return responseData;
    }

    public ResponseData getReportDetail(int reportId) {

        ResponseData reponseData = new ResponseData();
        int result = -1;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        List<Report> list = new ArrayList<Report>();

        try {
            String sql = "SELECT * FROM majlis_custom_reports WHERE REPORT_ID="+reportId;

            ResultSet rs = dbCon.search(con, sql);

            while (rs.next()) {
                Report report = new Report();
                report.setReportId(rs.getInt("REPORT_ID"));
                report.setReportName(rs.getString("REPORT_NAME"));
                report.setQuery(rs.getString("REPORT_QUERY"));
                 report.setStatus(rs.getInt("REPORT_STATUS"));

                list.add(report);

            }

            result = 1;
            reponseData.setResponseData(list);
        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        reponseData.setResponseCode(result);
        return reponseData;
    }

}
