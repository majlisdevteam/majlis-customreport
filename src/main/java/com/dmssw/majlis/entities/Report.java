/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dmssw.majlis.entities;

/**
 * <p>
 * <b>Title</b> Report
 * </p>
 * <p>
 * <b>Description</b> Enter Purpose of this class
 * </p>
 * <p>
 * <b>Company</b> Data Management Systems (PVT) Ltd
 * </p>
 * <p>
 * <b>Copyright</b> Copyright (c) 2015
 * </p>
 * @author Hashan Jayakody
 * @version 1.0
 */

public class Report {

    private int reportId;
    private String reportName;
    private String query;
    int status;

    public Report() {
    }

    public Report(int reportId, String reportName, String query) {
        this.reportId = reportId;
        this.reportName = reportName;
        this.query = query;
    }

    public Report(int reportId, String reportName, String query, int status) {
        this.reportId = reportId;
        this.reportName = reportName;
        this.query = query;
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }
    
    
}

