/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.services;

import com.dmssw.majlis.controllers.ReportController;
import com.dmssw.majlis.entities.Report;
import com.dmssw.orm.models.MajlisBirthdayTemplate;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * <p>
 * <b>Title</b> ReportService
 * </p>
 * <p>
 * <b>Description</b> Enter Purpose of this class
 * </p>
 * <p>
 * <b>Company</b> Data Management Systems (PVT) Ltd
 * </p>
 * <p>
 * <b>Copyright</b> Copyright (c) 2015
 * </p>
 *
 * @author Hashan Jayakody
 * @version 1.0
 */
@Path("/reports")
public class ReportService {

    @EJB
    ReportController reportController;

    @GET
    @Path("/getReport")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})

    public Response getReport(@QueryParam("query") String query) {
        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(reportController.getData(query)).build();

    }

    @POST
    @Path("/createReport")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createReport(Report report) {

        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(reportController.createReport(report)).build();
    }

    @POST
    @Path("/updateReport")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response updateReport(Report report) {

        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(reportController.updateReport(report)).build();
    }

    @GET
    @Path("/getReportList")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getReportList() {

        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(reportController.getReportList()).build();
    }
    
    @GET
    @Path("/getReportDetail")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getReportDetal(@QueryParam("reportId") int reportId) {

        System.out.println("Starting>>>");
        return Response.status(Response.Status.OK).entity(reportController.getReportDetail(reportId)).build();
    }

}
